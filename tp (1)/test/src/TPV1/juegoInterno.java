package TPV1;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class juegoInterno {
	
	private int intentos;
	private static String palabraAdivinar;
	private String palabraUsuario="";
    DateTimeFormatter Cronometro = DateTimeFormatter.ofPattern("HH:mm:ss");
    String [] ListaPal = 
		{"amigo","ayuda","audaz","andar","actor","ancho","astro","aviso","apego","atrio",
		"aunar","atroz","arena","atado","asear","apodo","abono","aleta","asado","apuro",
		"ajuar","acera","arado","airar","amiga","album","aupar","alear","aloja","arrea",
		"adobo","alojo","aleja","avalo","acuso","anota","afama","afamo","aboca","ajear",
		"acata","aduce","adulo","abren","aulas","apaga","apega","apago","avivo","apila",
		"azara","azaro","asumo","andan","abras","acuda","avisa","asuma","ataba","aguda",
		"andes","airar","alada","ayudo","apena","avara","amaso","acaba","armen","acojo",
		"armes","azota","azoto","amena","altos","aloco","ardan","amaga","bajar","breve",
		"burdo","bello","burla","busca","buena","beodo","bruja","basto","botar","bulto",
		"berma","barba","bando","bicho","beato","blusa","brujo","banca","burgo","broca",
		"bizco","boxeo","bases","bisel","bozal","broza","brete","borra","bonus","balda",
		"batea","bajos","bongo","berza","buzar","brego","brava","bruta","baldo","bajen",
		"beban","birlo","bulos","broto","boyen","bufan","besar","bailo","busco","causa",
		"calma","culpa","copia","clase","curar","corto","celos","ciclo","cifra","clima",
		"calle","cruel","citar","cesar","cerro","cabal","costo","carne","cisma","cavar",
		"coser","curvo","cruce","crudo","civil","canto","cesta","chulo","casco","cubil",
		"cursi","cabra","cutis","criba","caldo","cofre","criar","cejar","chola","calzo",
		"clava","calmo","cruza","copla","cafre","calza","cerda","caray","copar","corta",
		"choto","casca","cuche","corra","cenen","copes","carea","cebes","cunda","canas",
		"capta","crees","cuida","crear","clamo","cosan","ciemo","cacen","canta","cojeo",
		"caras","campa","citar","cardo","cuece","cucos","cesar","culpo","cocos","cuajo",
		"curen","cures","cacos","codea","coman","curar","comas","cuata","cagan","corre",
		"cedan","ciado","choco","ciega","causo","cruzo","cavos","cubre","decir","deber",
		"dolor","donar","danza","dosis","digno","doble","dardo","domar","dorso","dador",
		"duela","diana","dejan","deben","dudes","diere","disca","dejes","deban","decir",
		"doblo","desea","dotan","donar","dones","digna","domen","domes","dudan","duros",
		"draga","docta","dimos","estar","enojo","errar","ejote","entre","erebo","emito",
		"educo","errar","echen","eches","evado","evadi","elija","evita","entro","emana",
		"exijo","erogo","emite","forma","friki","firme","fluir","fijar","fuera","flojo",
		"fallo","falla","feria","fibra","fruta","farsa","frito","fardo","freno","flema",
		"feraz","fobia","farra","fleco","folio","fanal","feudo","finar","forja","fiemo",
		"fuere","finos","filmo","fisga","fasta","facas","floja","fiaca","ficho","frita",
		"finjo","finan","fajar","fundo","fulge","fluyo","frena","fugan","fetos","formo",
		"funge","hacer","haber","hasta","hacia","herir","humor","hueso","hosco","helar",
		"halar","hiato","hertz","hueva","honda","horro","hacho","hunda","halen","herir",
		"hacen","hueca","hemos","helar","halos","hayas","hayan","haber","hiles","hablo",
		"hojea","hojeo","hiere","halan","harta","igual","ileso","impar","izado","ibero",
		"islas","ilesa","izaba","jugar","juego","jalar","jaleo","jadeo","jarra","juzga",
		"joyel","jiras","juran","juzgo","kanes","nivel","nacer","notar","nicho","norte",
		"ninfa","nariz","naipe","numen","nidal","nublo","notar","nodos","nubes","narra",
		"nacer","naves","osado","obvio","obeso","ojear","ojera","odiar","ocupa","orina",
		"obras","ojete","osera","olote","opten","orlan","opuse","opero","ovino","ocupo",
		"oraba","ocres","orino","ollas","odiar","ocote","perro","parte","pasar","parar",
		"plano","pegar","padre","pelea","pulso","pilar","parca","pavor","pulir","prisa",
		"picar","porte","peste","plaga","patio","pillo","pinta","plata","peaje","porno",
		"penar","podar","pecar","prior","potro","punir","puzle","papeo","peana","pulla",
		"pifia","pampa","palmo","pulpa","panda","pingo","pulas","pocha","pegan","puyar",
		"punan","peras","pocos","placo","pongo","pagan","picar","ponga","pisan","poner",
		"pifio","pecan","pacta","pagar","pesco","peses","picos","pisar","pijos","pilla",
		"pecar","paran","pueda","pasan","pulan","pulsa","rueda","recto","rigor","rogar",
		"rival","resto","rezar","ruina","regio","reino","rollo","ruego","rodeo","robot",
		"rizar","riego","rasca","rumia","remar","rever","recta","ruedo","rabal","razia",
		"rinde","rigen","robar","rulos","rubia","reman","rayan","rozar","regia","salir",
		"sacar","suave","senda","suelo","stock","sutil","sitio","sabor","surco","salto",
		"semen","sesgo","saldo","segar","solar","sarna","salva","sacro","sarro","sapos",
		"sobar","suele","secar","suben","surjo","suman","sonar","salir","sagas","salas",
		"sumar","sanos","soban","sanan","sigue","somos","tocar","tarea","traer","texto",
		"tejer","trama","tirar","torpe","temer","tosco","tenso","turba","telar","traba",
		"trazo","trago","tasar","tropa","trono","talar","tramo","trola","tapiz","tarro",
		"tajar","tarta","terna","terno","tilde","tiros","tiesa","toros","turbo","tinta",
		"torno","usado","untar","ultra","valor","volar","vivir","visto","voraz","veraz",
		"vigor","venir","venia","virus","vivaz","vacuo","vulva","vello","vamos","vaina",
		"vence","votos","viaja","vemos","veces","vayan","wafle","yermo","yegua","zurdo",
		"zafar","zanco","zureo"};
	
	
//	//busca palabra random que el usuario debe adivinar
//	void CrearLista() {
//		String [] ListaPal = 
//				{"amigo","ayuda","audaz","andar","actor","ancho","astro","aviso","apego","atrio",
//				"aunar","atroz","arena","atado","asear","apodo","abono","aleta","asado","apuro",
//				"ajuar","acera","arado","airar","amiga","album","aupar","alear","aloja","arrea",
//				"adobo","alojo","aleja","avalo","acuso","anota","afama","afamo","aboca","ajear",
//				"acata","aduce","adulo","abren","aulas","apaga","apega","apago","avivo","apila",
//				"azara","azaro","asumo","andan","abras","acuda","avisa","asuma","ataba","aguda",
//				"andes","airar","alada","ayudo","apena","avara","amaso","acaba","armen","acojo",
//				"armes","azota","azoto","amena","altos","aloco","ardan","amaga","bajar","breve",
//				"burdo","bello","burla","busca","buena","beodo","bruja","basto","botar","bulto",
//				"berma","barba","bando","bicho","beato","blusa","brujo","banca","burgo","broca",
//				"bizco","boxeo","bases","bisel","bozal","broza","brete","borra","bonus","balda",
//				"batea","bajos","bongo","berza","buzar","brego","brava","bruta","baldo","bajen",
//				"beban","birlo","bulos","broto","boyen","bufan","besar","bailo","busco","causa",
//				"calma","culpa","copia","clase","curar","corto","celos","ciclo","cifra","clima",
//				"calle","cruel","citar","cesar","cerro","cabal","costo","carne","cisma","cavar",
//				"coser","curvo","cruce","crudo","civil","canto","cesta","chulo","casco","cubil",
//				"cursi","cabra","cutis","criba","caldo","cofre","criar","cejar","chola","calzo",
//				"clava","calmo","cruza","copla","cafre","calza","cerda","caray","copar","corta",
//				"choto","casca","cuche","corra","cenen","copes","carea","cebes","cunda","canas",
//				"capta","crees","cuida","crear","clamo","cosan","ciemo","cacen","canta","cojeo",
//				"caras","campa","citar","cardo","cuece","cucos","cesar","culpo","cocos","cuajo",
//				"curen","cures","cacos","codea","coman","curar","comas","cuata","cagan","corre",
//				"cedan","ciado","choco","ciega","causo","cruzo","cavos","cubre","decir","deber",
//				"dolor","donar","danza","dosis","digno","doble","dardo","domar","dorso","dador",
//				"duela","diana","dejan","deben","dudes","diere","disca","dejes","deban","decir",
//				"doblo","desea","dotan","donar","dones","digna","domen","domes","dudan","duros",
//				"draga","docta","dimos","estar","enojo","errar","ejote","entre","erebo","emito",
//				"educo","errar","echen","eches","evado","evadi","elija","evita","entro","emana",
//				"exijo","erogo","emite","forma","friki","firme","fluir","fijar","fuera","flojo",
//				"fallo","falla","feria","fibra","fruta","farsa","frito","fardo","freno","flema",
//				"feraz","fobia","farra","fleco","folio","fanal","feudo","finar","forja","fiemo",
//				"fuere","finos","filmo","fisga","fasta","facas","floja","fiaca","ficho","frita",
//				"finjo","finan","fajar","fundo","fulge","fluyo","frena","fugan","fetos","formo",
//				"funge","hacer","haber","hasta","hacia","herir","humor","hueso","hosco","helar",
//				"halar","hiato","hertz","hueva","honda","horro","hacho","hunda","halen","herir",
//				"hacen","hueca","hemos","helar","halos","hayas","hayan","haber","hiles","hablo",
//				"hojea","hojeo","hiere","halan","harta","igual","ileso","impar","izado","ibero",
//				"islas","ilesa","izaba","jugar","juego","jalar","jaleo","jadeo","jarra","juzga",
//				"joyel","jiras","juran","juzgo","kanes","nivel","nacer","notar","nicho","norte",
//				"ninfa","nariz","naipe","numen","nidal","nublo","notar","nodos","nubes","narra",
//				"nacer","naves","osado","obvio","obeso","ojear","ojera","odiar","ocupa","orina",
//				"obras","ojete","osera","olote","opten","orlan","opuse","opero","ovino","ocupo",
//				"oraba","ocres","orino","ollas","odiar","ocote","perro","parte","pasar","parar",
//				"plano","pegar","padre","pelea","pulso","pilar","parca","pavor","pulir","prisa",
//				"picar","porte","peste","plaga","patio","pillo","pinta","plata","peaje","porno",
//				"penar","podar","pecar","prior","potro","punir","puzle","papeo","peana","pulla",
//				"pifia","pampa","palmo","pulpa","panda","pingo","pulas","pocha","pegan","puyar",
//				"punan","peras","pocos","placo","pongo","pagan","picar","ponga","pisan","poner",
//				"pifio","pecan","pacta","pagar","pesco","peses","picos","pisar","pijos","pilla",
//				"pecar","paran","pueda","pasan","pulan","pulsa","rueda","recto","rigor","rogar",
//				"rival","resto","rezar","ruina","regio","reino","rollo","ruego","rodeo","robot",
//				"rizar","riego","rasca","rumia","remar","rever","recta","ruedo","rabal","razia",
//				"rinde","rigen","robar","rulos","rubia","reman","rayan","rozar","regia","salir",
//				"sacar","suave","senda","suelo","stock","sutil","sitio","sabor","surco","salto",
//				"semen","sesgo","saldo","segar","solar","sarna","salva","sacro","sarro","sapos",
//				"sobar","suele","secar","suben","surjo","suman","sonar","salir","sagas","salas",
//				"sumar","sanos","soban","sanan","sigue","somos","tocar","tarea","traer","texto",
//				"tejer","trama","tirar","torpe","temer","tosco","tenso","turba","telar","traba",
//				"trazo","trago","tasar","tropa","trono","talar","tramo","trola","tapiz","tarro",
//				"tajar","tarta","terna","terno","tilde","tiros","tiesa","toros","turbo","tinta",
//				"torno","usado","untar","ultra","valor","volar","vivir","visto","voraz","veraz",
//				"vigor","venir","venia","virus","vivaz","vacuo","vulva","vello","vamos","vaina",
//				"vence","votos","viaja","vemos","veces","vayan","wafle","yermo","yegua","zurdo",
//				"zafar","zanco","zureo"};
//	}
	public juegoInterno() {
		palabraAdivinar = damePalabra();
		System.out.println(palabraAdivinar);
	}
	//Da una palabra random que el usuario debe adivinar
		public String damePalabra() {
				palabraAdivinar = buscaPalabraRandom(ListaPal);
				return palabraAdivinar;
			}
		// Busca una palabra random
		private String buscaPalabraRandom(String[] listaPal2) {
			Random random = new Random();
			int index = random.nextInt(listaPal2.length);
			return ListaPal[index];
		}
	
	
	//busca que la palabra que ingreso el usuario este en la lista
	boolean buscarPalabra(String PalabraUsuario) {
		return false;
	}
	

	public enum estadoLetra{Correcto, CorrectoMalUbicado, Mal};
	
	public static estadoLetra[] compararPalabra(String s) {
		estadoLetra [] palabra = {estadoLetra.Mal, estadoLetra.Mal,estadoLetra.Mal,estadoLetra.Mal,estadoLetra.Mal};
		for (int i = 0; i < palabraAdivinar.length(); i++) {
			if (palabraAdivinar.charAt(i) == s.charAt(i)) {
				palabra[i] = estadoLetra.Correcto;
			
			}else if(ContieneEstaLetra(palabraAdivinar,s.charAt(i))) { 
				palabra[i]= estadoLetra.CorrectoMalUbicado;
			
			}else if (palabraAdivinar.charAt(i) != s.charAt(i) && !ContieneEstaLetra(palabraAdivinar,s.charAt(i))) { 
				palabra[i] = estadoLetra.Mal;
		
			}
		}
	return palabra;	
		} 
		
	
	private static boolean ContieneEstaLetra(String palabra ,char string) {
		boolean flag = false;
		
		for (int i = 0; i < palabra.length(); i++) {
			flag = flag || palabra.charAt(i) == string ;
		}
		return flag;
	}
	void iniciarTiempo() {
		String empezo = Cronometro.format(LocalDateTime.now());
	}

	void finalizarTiempo() {
	   String Termino = Cronometro.format(LocalDateTime.now());

	}
	
	//devuelve puntaje
	int darResultado() {
		return 0;
	}
	
	//une los char para formar la palabra
	public String compilarPalabra(char letra) {
		if (palabraUsuario.length()<5)
			palabraUsuario+=letra;
		return palabraUsuario;
	}
	
	public String borrameEseCoso(){
		 return palabraUsuario="";
	}
	
	public String borrarletra() {
		if(palabraUsuario!=null) {
			String nueva="";
			for (int i = 0;i< palabraUsuario.length()-1;i++ ) {
				nueva+= palabraUsuario.charAt(i);
			}
			palabraUsuario = nueva;
		}
		
		return palabraUsuario;
	}
	
	public static void main(String[] args) {
		String palaba = "mapas";
		estadoLetra [] as = compararPalabra(palaba);
		for (int i = 0; i < palabraAdivinar.length(); i++) {
			
			System.out.println(as[i].toString());
		}
	}

	
}
